from math import sin, radians, sqrt, cos

from app.pad654.fishing_net.fishing_net import FishingNet
from app.pad654.ship.ship import Ship


class FishingTime:
    def __init__(self, ship_one: Ship, ship_two: Ship, fishing_net: FishingNet):
        super().__init__()
        self.ship_one = ship_one
        self.ship_two = ship_two
        self.fishing_net = fishing_net
        self._difference = 0
        self._gama = 0
        self._km_sailed = 1
        self.minutes_ship_one = 0
        self.minutes_ship_two = 0
        self.fishing_net_distance = 0

    def _gama_and_difference_degrees(self) -> list:
        self._difference = abs(self.ship_one.get_direction() - self.ship_two.get_direction())
        self._gama = (180 - self._difference) / 2
        return [self._gama, self._difference]

    def find_time_to_break(self) -> float:
        self._gama_and_difference_degrees()
        while self._fishing_net_do_not_break_yet():
            self._ship_distance_has_sailed_on_troubled_waters()
            self._calculate_fishing_net_distance()
            self._verify_minutes()
            self._km_sailed += 1
            if self._gama == 0:
                self.minutes_ship_one = 12.8
                self.minutes_ship_two = 12.8
                break
            if self._difference == 0:
                return float('inf')
        return float(round(self.minutes_ship_one, 2))

    def _calculate_fishing_net_distance(self) -> float:
        self.fishing_net_distance = self.ship_one.get_distance() ** 2 + self.ship_two.get_distance() ** 2 - 2 * \
                        self.ship_one.get_distance() * self.ship_two.get_distance() * cos(radians(self._difference))
        self.fishing_net_distance = sqrt(self.fishing_net_distance)
        return self.fishing_net_distance

    def _verify_minutes(self) -> list:
        time_ship_one = self.ship_one.get_distance() / self.ship_one.get_speed()
        time_ship_two = self.ship_one.get_distance() / self.ship_two.get_speed()
        # Ship One
        time_in_minutes_ship_one = time_ship_one * 60
        # Ship Two
        time_in_minutes_ship_two = time_ship_two * 60
        self.minutes_ship_one = time_in_minutes_ship_one
        self.minutes_ship_two = time_in_minutes_ship_two
        return [self.minutes_ship_one, self.minutes_ship_two]

    def _fishing_net_do_not_break_yet(self) -> bool:
        if self.fishing_net_distance >= self.fishing_net.get_fishing_net_length():
            return False
        return True

    def _ship_distance_has_sailed_on_troubled_waters(self) -> list:
        if self._difference != 0:
            self.ship_one.set_distance((self._km_sailed * sin(radians(self._gama))) / sin(radians(self._difference)))
            self.ship_two.set_distance((self._km_sailed * sin(radians(self._gama))) / sin(radians(self._difference)))
            return [self.ship_one.get_distance(), self.ship_two.get_distance()]
        else:
            return [float('inf')]
