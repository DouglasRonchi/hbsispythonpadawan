class FishingNet:
    def __init__(self):
        self._length = 40 * 1.609

    def get_fishing_net_length(self) -> float:
        return self._length

    def set_fishing_net_length(self, lenght):
        self._length = lenght
