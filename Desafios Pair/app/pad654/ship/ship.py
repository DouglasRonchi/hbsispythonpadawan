class Ship:
    def __init__(self, speed, direction):
        self._speed = speed
        self._direction = direction
        self._distance = 0

    def get_speed(self) -> int:
        return self._speed

    def get_direction(self) -> int:
        return self._direction

    def get_distance(self) -> float:
        return self._distance

    def set_speed(self, value):
        self._speed = value

    def set_direction(self, value):
        self._direction = value

    def set_distance(self, value):
        self._distance = value
