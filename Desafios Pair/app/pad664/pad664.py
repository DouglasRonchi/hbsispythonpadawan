import re


def count_smileys(arr):
    return sum(1 if re.search('^((?P<eyes>[:,;])?(?P<nose>[-,~])?(?P<mouth>[),D]))$', data)
                    and len(data) > 1 else 0 for data in arr)

