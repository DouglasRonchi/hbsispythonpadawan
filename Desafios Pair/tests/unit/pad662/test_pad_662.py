import unittest

from app.pad662.pad662 import qi_verification


class TestPad662(unittest.TestCase):

    def test_iq_test(self):
        self.assertEqual(qi_verification("2 4 7 8 10"), 3)
        self.assertEqual(qi_verification("1 1 1 1 1 2 1 1 1 3"), 6)
        self.assertEqual(qi_verification("1 1 1 1 1 2"), 6)
        self.assertEqual(qi_verification("2 1 2 2 2 2 2"), 2)
        self.assertEqual(qi_verification("0 0 2 2 1"), 5)
