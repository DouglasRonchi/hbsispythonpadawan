import unittest

from app.pad664.pad664 import count_smileys


class TestPad664(unittest.TestCase):

    def test_count_smileys(self):
        self.assertEqual(count_smileys([':D', ':~)', ';~D', ':)']), 4)
        self.assertEqual(count_smileys([':)', ';(', ';}', ':-D']), 2)
        self.assertEqual(count_smileys([';D', ':-(', ':-)', ';~)']), 3)
        self.assertEqual(count_smileys([';]', ':[', ';*', ':$', ';-D']), 1)
        self.assertEqual(count_smileys(['-;]']), 0)
        self.assertEqual(count_smileys([';]-']), 0)
        self.assertEqual(count_smileys([';.]']), 0)
        self.assertEqual(count_smileys(['D:']), 0)

