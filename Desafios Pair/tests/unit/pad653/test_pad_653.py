import unittest

import pytest

from app.pad653.pad653 import Plant


class TestPad653(unittest.TestCase):

    def test_growing_plant(self):
        plant = Plant()
        self.assertEqual(plant.growing_plant(100, 10, 910), 10)
        self.assertEqual(plant.growing_plant(10, 9, 4), 1)
        self.assertEqual(plant.growing_plant(50, 10, 500), 13)

    def test_exception_upspeed(self):
        plant = Plant()
        with pytest.raises(Exception) as ex:
            plant.growing_plant(150, 10, 910)
        self.assertEqual(ex.value.args[0], "UpSpeed must be between 5 and 100")

    def test_exception_downspeed(self):
        plant = Plant()
        with pytest.raises(Exception) as ex:
            plant.growing_plant(100, 100, 910)
        self.assertEqual(ex.value.args[0], "DownSpeed must be between 2 and UpSpeed value")

    def test_exception_desired_height(self):
        plant = Plant()
        with pytest.raises(Exception) as ex:
            plant.growing_plant(100, 10, 1500)
        self.assertEqual(ex.value.args[0], "Desired Height must be between 4 and 1000")
