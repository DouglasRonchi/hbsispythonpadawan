import unittest

from app.pad654.ship.ship import Ship


class MyTestCase(unittest.TestCase):
    def test_if_shop_is_a_instance_of_ship_class(self):
        ship = Ship(105, 0)
        self.assertIsInstance(ship, Ship)

    def test_getters(self):
        ship = Ship(150, 0)
        self.assertEqual(ship.get_speed(), 150)
        self.assertEqual(ship.get_direction(), 0)
        self.assertEqual(ship.get_distance(), 0)

    def test_set_speed(self):
        ship = Ship(150, 0)
        ship.set_speed(200)
        self.assertEqual(ship._speed, 200)

    def test_set_direction(self):
        ship = Ship(150, 0)
        ship.set_direction(90)
        self.assertEqual(ship._direction, 90)

    def test_set_distance(self):
        ship = Ship(150, 0)
        ship.set_distance(45.25)
        self.assertEqual(ship._distance, 45.25)

