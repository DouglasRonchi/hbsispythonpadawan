import unittest
from unittest.mock import Mock

from app.pad654.fishing_time.fishing_time import FishingTime


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_ship_one = Mock()
        self.mock_ship_two = Mock()
        self.mock_fishing_net = Mock()
        # Ships
        self.mock_ship_one.get_speed.return_value = 150
        self.mock_ship_one.get_direction.return_value = 0
        self.mock_ship_one.get_distance.return_value = 46
        self.mock_ship_two.get_speed.return_value = 150
        self.mock_ship_two.get_direction.return_value = 90
        self.mock_ship_two.get_distance.return_value = 46

        self.mock_fishing_net.get_fishing_net_length.return_value = 64

        self.fishing_time = FishingTime(self.mock_ship_one, self.mock_ship_two, self.mock_fishing_net)

    def test_if_fishing_time_is_a_instance_of_fishing_time(self):
        self.assertIsInstance(self.fishing_time, FishingTime)

    def test_gama_and_difference_degrees(self):
        result = self.fishing_time._gama_and_difference_degrees()
        self.assertEqual(result, [45.0, 90])

    def test__calculate_fishing_net_distance(self):
        result = self.fishing_time._calculate_fishing_net_distance()
        self.assertEqual(result, 0.0)

    def test_find_time_to_break(self):
        self.fishing_time = FishingTime(self.mock_ship_one, self.mock_ship_two, self.mock_fishing_net)
        result = self.fishing_time.find_time_to_break()
        self.assertEqual(result, 18.4)

    def test_verify_minutes(self):
        result = self.fishing_time._verify_minutes()
        self.assertEqual(result, [18.4, 18.4])

    def test_fishing_net_do_not_break_yet_true(self):
        result = self.fishing_time._fishing_net_do_not_break_yet()
        self.assertEqual(result, True)

    def test_fishing_net_do_not_break_yet_false(self):
        self.fishing_time.fishing_net_distance = 64
        result = self.fishing_time._fishing_net_do_not_break_yet()
        self.assertEqual(result, False)

    def test_if_gama_its_equal_to_180(self):
        mock_ship_one = Mock()
        mock_ship_two = Mock()
        mock_fishing_net = Mock()
        fishing_time = FishingTime(mock_ship_one, mock_ship_two, mock_fishing_net)
        # Ships
        mock_ship_one.get_speed.return_value = 150
        mock_ship_one.get_direction.return_value = 0
        mock_ship_one.get_distance.return_value = 45
        mock_ship_two.get_speed.return_value = 150
        mock_ship_two.get_direction.return_value = 180
        mock_ship_two.get_distance.return_value = 45
        # FishingNet
        mock_fishing_net.get_fishing_net_length.return_value = 40 * 1.609

        self.assertEqual(fishing_time.find_time_to_break(), 12.8)

    def test_if_ships_are_sailing_to_the_same_way(self):
        mock_ship_one = Mock()
        mock_ship_two = Mock()
        mock_fishing_net = Mock()
        fishing_time = FishingTime(mock_ship_one, mock_ship_two, mock_fishing_net)
        # Ships
        mock_ship_one.get_speed.return_value = 150
        mock_ship_one.get_direction.return_value = 0
        mock_ship_one.get_distance.return_value = 45
        mock_ship_two.get_speed.return_value = 150
        mock_ship_two.get_direction.return_value = 0
        mock_ship_two.get_distance.return_value = 45
        # FishingNet
        mock_fishing_net.get_fishing_net_length.return_value = 40 * 1.609

        self.assertEqual(fishing_time.find_time_to_break(), float('inf'))


