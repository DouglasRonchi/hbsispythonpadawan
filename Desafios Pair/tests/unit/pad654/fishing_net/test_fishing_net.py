import unittest

from app.pad654.fishing_net.fishing_net import FishingNet


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.fishing_net = FishingNet()

    def test_if_fishing_net_is_a_instance_of_fishing_net_class(self):
        self.assertIsInstance(self.fishing_net, FishingNet)

    def test_get_fishing_net_length(self):
        self.assertEqual(self.fishing_net.get_fishing_net_length(), 64.36)

    def test_set_fishing_net_length(self):
        self.fishing_net.set_fishing_net_length(25)
        self.assertEqual(self.fishing_net._length, 25)

